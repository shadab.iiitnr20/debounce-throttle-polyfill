console.log("Hello World");

const btn = document.getElementById("btn");
const btnClicked = document.getElementById("btnClicked");
const btnTriggered = document.getElementById("btnTriggered");

var btnCount = 0;
var trgCount = 0;

const myDebounce = (cb, d) => {
  let timer;
  return function (...args) {
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      cb(...args);
    }, d);
  };
};

// const debounce = myDebounce(() => {
//   btnTriggered.innerHTML = ++trgCount;
// }, 800);

const myThrottle = (cb, d) => {
  let last = 0;
  return function (...args) {
    let now = new Date().getTime();
    if (now - last < d) return;
    last = now;
    return cb(...args);
  };
};

const throttle = myThrottle(() => {
  btnTriggered.innerHTML = ++trgCount;
}, 1000);

btn.addEventListener("click", () => {
  btnClicked.innerHTML = ++btnCount;
  //debounce();
  throttle();
});
